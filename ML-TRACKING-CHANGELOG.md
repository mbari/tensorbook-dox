# Changelog
All notable changes to the ml-tracking project will be documented in this file.
Note the this project depends on software spread over many directories in ~/prj.local/BioEng

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2021-05-25
### Added
- VS Code
- Google Chrome
- Installed mumble
- added tensorbook-march-2021 branch to mwt-control
- Major rework of of supervisor code into boxview GUI
- Scripts updated to take deployment directory as first argument to run_control 
- Synchronized class label list with names from cvision model
- Added object acquire and track centering to supervisor
- Added supervisor_cfg lcm type to log acquire and track automated control settings
- Added tensorbook-may-2021 branch to mwt-control
- Added fully automatic Search-Acquire-Track mode to supervisor and boxview

### Changed
- Switched mwt-apps back to master branch and pulled in changes to support opencv4
- Pulled in the latest changes to mwt-control and mwt-apps to support SEARCH MODE
- Using VS code now for python and C++ development
- ```sudo apt-get update && sudo apt-get upgrade```
- ```sudo apt-get update && sudo apt-get dist-upgrade```
- ```sudo apt-get install nvidia-utils-460```
- ```sudo apt install nvidia-driver-460```
- Removed moxa Real Port Serial Driver and switchted light control to UDP


## [1.0.0] - 2020-07-25
### Added
- Consolidation of all ml-tracking related software onto the tensorbook
- Pulled in significantly improved models frpom CVision AI

### Changed
- Scripts now are adapted to use proman
