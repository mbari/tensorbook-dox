# Tensorbook ML-Tracking Setup

## Ubuntu Install

1. Download Ubuntu Desktop 20.04 and create a bootable USB drive
2. Plug in drive to tensorbook and press F12 after poweron to get to boot menu
3. Select boot from file option, browse to USB drive->EFI->BOOT, then select grubx64.efi
4. Boot Ubuntu with safe graphics
5. Follow installation instructions
6. Create user: John Q. Ops, username = ops, password ~ ask Paul or Mike

## Cool Background (Optional)

1. Download a nice desktop background from here: http://www.digitalblasphemy.com
2. If MBARI wallpapers are more your thing: https://www.mbari.org/products/wallpaper/
3. If you ask really nicely, Joost might make you a Blender render of copepods :)
3. Right-click the desktop, select change background, and select your image of choice

## Setup Gedit

1. Open gedit
2. Click on the button with three horizontal lines
3. Set tabs to 4
4. select replace tabs with spaces
5. Select Preferences->Fonts & Colors
6. Click on Oblivion


## Install Vim

`sudo apt-get install vim`

## Install locate

`sudo apt-get install mlocate`

## Install ZeroMQ

`sudo apt-get install libzmq3-dev`

## Install Filezilla

`sudo apt-get install filezilla`

## Install Terminator

`sudo apt-get install terminator`


## Install Meld

`sudo apt-get install meld`

## Install VLC

`sudo apt-get install vlc`

## Install restricted extras

`sudo apt-get install ubuntu-restricted-extras`

If you don't want Microsoft fonts intstalled, not accept the EULA when prompted

## Install doxygen

`sudo apt-get install doxygen`

## Install ssh server

`sudo apt-get install ssh`

## Install wireshark (handy for troubleshooting LCM)

`sudu apt-get install wireshark`

## Install psensor (GUI for system temps)

`sudo apt-get install psensor`

## Install Typora for markdown fun

```
# or run:
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA300B7755AFCFAE

wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -

# add Typora's repository

sudo add-apt-repository 'deb https://typora.io/linux ./'

sudo apt-get update

# install typora

sudo apt-get install typora
```

## Install Google Chrome

Download from here: https://www.google.com/chrome/

## Install Moxa Serial Server (Deprecated in favor of using UDP)

Download linux drivers from https://www.moxa.com/en/support/product-support/software-and-documentation/search?psid=50254 and extract them to ```~/prj.local/BioEng/installed``` Currently we are using the version for kernel 4.x but it is worth trying the kernel for 5.x

Then do

```
ops@tensorbook1:~/prj.local/BioEng/installed/ $ cd moxa
ops@tensorbook1:~/prj.local/BioEng/installed/moxa $ sudo ./mxinst
ops@tensorbook1:~/prj.local/BioEng/installed/moxa $ sudo ./mxaddsvr 192.168.1.83 2
```

Make sure to add user ops to dialout group and restart shell

## Using Moxa with UDP

This is far easier than the serial setup and avoids the long delay during system shutdown
All that is needed is to configure the server through the web UI to use UDP for each port

1. browse to 192.168.1.83
2. Enter the admin password (the default is moxa)
3. Expand "Operating Settings" 
4. For each port set the operating mode to UDP, the ip address to 192.168.1.83, and the port to a unique values starting at 4001
5. Then to set the lights you can use the script ~/prj.local/BioEng/scripts/set_light_output

## Create Dev Directories

```
cd /home/ops
mkdir prj.local
cd prj.local
mkdir BioEng
```

## Setup profile and a few handy aliases to bashrc

alias gosoft='cd /home/ops/prj.local/BioEng'


## Install lambda stack: https://lambdalabs.com/lambda-stack-deep-learning-software

```
LAMBDA_REPO=$(mktemp) && \
wget -O${LAMBDA_REPO} https://lambdalabs.com/static/misc/lambda-stack-repo.deb && \
sudo dpkg -i ${LAMBDA_REPO} && rm -f ${LAMBDA_REPO} && \
sudo apt-get update && sudo apt-get install -y lambda-stack-cuda
```

## Setup path for third-party software installs

```
ops@tensorbook1:~$ gosoft
ops@tensorbook1:~/prj.local/BioEng$ mkdir installed
ops@tensorbook1:~/prj.local/BioEng$ cd installed
```

## Setup python3 and virtual environments

This is a mix of what seems to be the lambda stack setup and the guide from
pyimagesearch: https://www.pyimagesearch.com/2017/09/25/configuring-ubuntu-for-deep-learning-with-python/

```
ops@tensorbook1:~$ sudo pip3 install virtualenv virtualenvwrapper
ops@tensorbook1:~$ echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.bashrc
ops@tensorbook1:~$ echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.bashrc
ops@tensorbook1:~$ echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> ~/.bashrc
ops@tensorbook1:~$ echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
ops@tensorbook1:~$ source .bashrc
```

Now to actually create the virtualenv

```
ops@tensorbook1:~$ mkvirtualenv ml-tracking -p python3
```


Install typical python packages

```
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install numpy
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install scipy
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install pyqt5
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install pyqtgraph
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install pyserial
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install epydoc
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install pyopengl
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install matplotlib
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install psutil
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install nvgpu
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install openpiv
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install pykalman
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install imageio-ffmpeg
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install screeninfo
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/installed$ pip install pi-py
```


## Download and Install PyCharm (Deprecated)

```
ops@tensorbook1:~/prj.local/BioEng/installed$ mv ~/Downloads/pycharm-community-2020.1.4.tar.gz .
ops@tensorbook1:~/prj.local/BioEng/installed$ tar xvzf pycharm-community-2020.1.4.tar.gz
```

## Download and install VS Code (Recommended)

Download .deb package from: https://code.visualstudio.com/docs/?dv=linux64_deb into ```ops@tensorbook1:~/prj.local/BioEng/installed```

```
ops@tensorbook1:~/prj.local/BioEng/installed$ sudo apt-get install ./code_1.52.1-1608136922_amd64.deb
```

Useful extensions to add to Code:

- CMake Tools
- ESLint
- Jupyter
- PYQT Integration
- Python
- Python Docstring Generator
- Rainbow CSV

## ML-Tracking Setup

### LCM Setup

```
ops@tensorbook1:~$ sudo apt-get install libglib2.0-dev
ops@tensorbook1:~$ sudo apt-get install default-jdk
ops@tensorbook1:~$ sudo apt-get install python-all-dev
ops@tensorbook1:~$ sudo apt-get install liblua5.1-dev
ops@tensorbook1:~$ sudo apt-get install golang
ops@tensorbook1:~$ pip install epydoc
ops@tensorbook1:~$ workon ml-tracking
(ml-tracking) ops@tensorbook1:~$ gosoft
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng$ git clone https://github.com/lcm-proj/lcm.git
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng$ cd lcm
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm$ mkdir build
(ml-tracking) ps@tensorbook1:~/prj.local/BioEng/lcm$ cd build
(ml-tracking) ps@tensorbook1:~/prj.local/BioEng/lcm/build$ cmake ..
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm/build$ make -j4
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm/build$ sudo make install
```

Adding lcm to virtualenv install requires one more step

```
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm/build$ cd ../lcm-python
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm/lcm-python$ python setup.py install
```

Adding lcm to python 2.7 install as above

```
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng/lcm/build$ deactivate
ops@tensorbook1:~/prj.local/BioEng/lcm/lcm-python$ sudo python setup.py install
```


Make sure to add the library path for LCM to 

`ops@tensorbook1:~/prj.local/BioEng/lcm$ sudo bash -c 'echo /usr/local/lib > /etc/ld.so.conf.d/lcm.conf'`

Test LCM

```
ops@tensorbook1:~/prj.local/BioEng/lcm/lcm-python$ cd ../examples/lcm-spy
ops@tensorbook1:~/prj.local/BioEng/lcm/examples/lcm-spy$ ./runspy.sh
```

Play a log file from one of the many MiniROV dives and confirm you see LCM messages in spy

### Docker and nvidia-docker setup: https://github.com/NVIDIA/nvidia-docker

#### Install docker and add ops to docker group

```
ops@tensorbook1:~$ sudo apt-get install docker.io
ops@tensorbook1:~$ sudo usermod -a -G docker ops
```

#### Install nvidia-docker

```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker
```

Need to restart the laptop at this point (not sure why)
Desktop will bit a bit slow after logging in as docker does whatever it does

#### Test Docker Setup

```
ops@tensorbook1:~$ docker run --gpus all nvidia/cuda:10.0-base nvidia-smi
Unable to find image 'nvidia/cuda:10.0-base' locally
10.0-base: Pulling from nvidia/cuda
7ddbc47eeb70: Pull complete 
c1bbdc448b72: Pull complete 
8c3b70e39044: Pull complete 
45d437916d57: Pull complete 
d8f1569ddae6: Pull complete 
de5a2c57c41d: Pull complete 
ea6f04a00543: Pull complete 
Digest: sha256:e6e1001f286d084f8a3aea991afbcfe92cd389ad1f4883491d43631f152f175e
Status: Downloaded newer image for nvidia/cuda:10.0-base
Sat Jul 25 01:01:12 2020       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.57       Driver Version: 450.57       CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  GeForce RTX 207...  On   | 00000000:01:00.0 Off |                  N/A |
| N/A   46C    P8     4W /  N/A |    184MiB /  7982MiB |     15%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
+-----------------------------------------------------------------------------+
```

### Install CVision AI images

`ops@tensorbook1:~$ docker pull cvisionai/mbari_tracking:2020.07.19`
`ops@tensorbook1:~$ docker pull cvisionai/mbari_tracking_test:2020.07.19`
`ops@tensorbook1:~$ docker pull cvisionai/lcm_image:2020.07.19`

### Install Vimba

Accept license and download Vimba 4.0 from here: https://www.alliedvision.com/en/products/software.html

Follow steps for installing Vimba under Linux here: https://cdn.alliedvision.com/fileadmin/content/documents/products/software/software/Vimba/appnote/Vimba_installation_under_Linux.pdf

Put Vimba in /home/ops/prj.local/BioEng/installed

Create a symlink to scripts:

`ops@tensorbook1:~/prj.local/BioEng/scripts$ ln -s ../installed/Vimba_4_0/Tools/Viewer/Bin/x86_64bit/VimbaViewer VimbaViewer`



### Download Python Camera Acquisition Software

```
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng$ pip install pymba
(ml-tracking) ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/mantaview.git
```

### Download MBARI ML-Tracking Software

```
ops@tensorbook1:~$ gosoft
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/mwt-control.git
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/mwt-apps.git
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/mwt-lcm-types.git
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/pcon-lib.git
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/ml-boxview.git
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/lcm-video-export.git
ops@tensorbook1:~/prj.local/BioEng$ git clone https://pldr@bitbucket.org/mbari/tensorbook-scripts.git scripts

```

### Download ML-Tracking Software Additional Libraries

```
(base) ops@tensorbook1:~/prj.local/BioEng$ git clone https://github.com/libbot2/libbot2.git

```

### OpenCV Setup

```
ops@tensorbook1:~/prj.local/BioEng$ cd installed/
ops@tensorbook1:~/prj.local/BioEng/installed$ wget https://github.com/milq/milq/raw/master/scripts/bash/install-opencv.sh
ops@tensorbook1:~/prj.local/BioEng/installed$ vi install-opencv.sh
```

#### Cmake command to configure OpenCV with python and contrib

We want to build opencv for system-wide use, so deactive any user virtualenvs so that which python3 returns the system-wide python3 installation from the lambda stack. Update the 
cmake command to the following:

cmake -DWITH_QT=ON -DWITH_OPENGL=ON -DFORCE_VTK=ON -DWITH_TBB=ON -DWITH_GDAL=ON -DWITH_XINE=ON -DENABLE_PRECOMPILED_HEADERS=OFF -DBUILD_NEW_PYTHON_SUPPORT=ON -DBUILD_opencv_python3=ON -DHAVE_opencv_python3=ON -DPYTHON_DEFAULT_EXECUTABLE=$(which python) -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules ..

```
ops@tensorbook1:~/prj.local/BioEng/scripts$ chmod a+x install-opencv.sh
ops@tensorbook1:~/prj.local/BioEng/scripts$ ./install-opencv.sh
```


Copy cv2 module to virtualenv

`cp -rf /usr/local/lib/python3.8/dist-packages/cv2 ~/.virtualenvs/ml-tracking/lib/python3.8/site-packages/`

#### Create opencv4.pc in /usr/local/lib/pkgconfig

```
# Package Information for pkg-config

prefix=/usr/local
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir_old=${prefix}/include/opencv4
includedir_new=${prefix}/include/opencv4/opencv2

Name: OpenCV
Description: Open Source Computer Vision Library
Version: 4.2
Libs: -L${exec_prefix}/lib -lopencv_stitching -lopencv_superres -lopencv_videostab -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired -lopencv_ccalib -lopencv_cvv -lopencv_dnn_objdetect -lopencv_dpm -lopencv_face -lopencv_fuzzy -lopencv_hdf -lopencv_hfs -lopencv_img_hash -lopencv_line_descriptor -lopencv_optflow -lopencv_reg -lopencv_rgbd -lopencv_saliency -lopencv_stereo -lopencv_structured_light -lopencv_viz -lopencv_phase_unwrapping -lopencv_surface_matching -lopencv_tracking -lopencv_datasets -lopencv_text -lopencv_highgui -lopencv_videoio -lopencv_dnn -lopencv_plot -lopencv_xfeatures2d -lopencv_shape -lopencv_video -lopencv_ml -lopencv_ximgproc -lopencv_xobjdetect -lopencv_objdetect -lopencv_calib3d -lopencv_imgcodecs -lopencv_features2d -lopencv_flann -lopencv_xphoto -lopencv_photo -lopencv_imgproc -lopencv_core
Libs.private: -ldl -lm -lpthread -lrt -L/usr/lib/x86_64-linux-gnu -lGL -lGLU
Cflags: -I${includedir_old} -I${includedir_new}
```

### Install MBARI ML-tracking Software

#### libbot2

`ops@tensorbook1:~/prj.local/BioEng$ cd libbot2`

Edit tobuild.txt to look like this:

```
# list of subdirectories to build, one one each line.  Empty lines
# and lines starting with '#' are ignored

bot2-core
#bot2-vis
bot2-procman
#bot2-lcmgl
bot2-lcm-utils
#bot2-param
#bot2-frames
```

Need python-gtk which is no longer part of 20.04 so install it manually:

```
ops@tensorbook1:~/prj.local/BioEng/installed$ wget http://us.archive.ubuntu.com/ubuntu/pool/universe/p/pygtk/python-gtk2_2.24.0-6_amd64.deb
ops@tensorbook1:~/prj.local/BioEng/installed$ sudo apt-get install ./python-gtk2_2.24.0-6_amd64.deb
```


Build the code and install in one command, otherwise install goes to the wrong place

`(base) ops@tensorbook1:~/prj.local/BioEng/libbot2$ sudo make BUILD_PREFIX=/usr/local`


#### mwt-control

Note that mwt-control resuires pcon-lib to be at ../

Generate or regenerate the lcm header files

```
ops@tensorbook1:~/prj.local/BioEng$ cd mwt-lcm-types
ops@tensorbook1:~/prj.local/BioEng/mwt-lcm-types$ lcm-gen -x --cpp-hpath ../mwt-control/src/control *.lcm
ops@tensorbook1:~/prj.local/BioEng/mwt-lcm-types$ lcm-gen -x --cpp-hpath ../mwt-control/src/control deprecated/*.lcm
ops@tensorbook1:~/prj.local/BioEng/mwt-lcm-types$ lcm-gen -x --cpp-hpath ../mwt-control/src/intfc/mini_rov *.lcm
ops@tensorbook1:~/prj.local/BioEng/mwt-lcm-types$ lcm-gen -x --cpp-hpath ../mwt-control/src/intfc/mini_rov  deprecated/*.lcm
ops@tensorbook1:~/prj.local/BioEng/mwt-lcm-types$ lcm-gen -x --cpp-hpath ../mwt-control/src/utils/gui_bridge *.lcm
ops@tensorbook1:~/prj.local/BioEng/mwt-lcm-types$ lcm-gen -x --cpp-hpath ../mwt-control/src/utils/gui_bridge deprecated/*.lcm
```

Once the headers are generated build the repo

```
ops@tensorbook1:~/prj.local/BioEng$ cd mwt-control
ops@tensorbook1:~/prj.local/BioEng/mwt-control$ make -j4
```

From what I can tell, the repo is missing the setup to build the gui. I copied this over
from rock-nuc-1 and it seems to work. We should update the repo to allow for building
the GUI with QtCreator

#### mwt-apps


Several changes were required to use mwt-apps with OpenCV 4. I created a new branch opencv4 in the repo and committed these changes


```
ops@tensorbook1:~/prj.local/BioEng$ cd mwt-apps
ops@tensorbook1:~/prj.local/BioEng/mwt-apps$ make -j4


```

#### Git Setup

- in ~/.gitconfig 
```
[user]
        email = [email]
        name = [name]
[core]
        autocrlf = input

[credential]
        helper = cache --timeout=43200
```

- Create script to check repo status in `~/prj.local/BioEng/scripts`:

```
#!/bin/sh

#https://stackoverflow.com/questions/3258243/check-if-pull-needed-in-git

git fetch

UPSTREAM=${1:-'@{u}'}
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
    echo "Up-to-date"
elif [ $LOCAL = $BASE ]; then
    echo "Need to pull"
elif [ $REMOTE = $BASE ]; then
    echo "Need to push"
else
    echo "Diverged"
fi
```













