# Tracking tests 08/20/2020

1. stereo_sync.py - killed after about 30 minutes of tracking, likely memory leak issue. Confirmed memory leak issue, looks to be about 
2. tracker output is much smoother than kcf.  **big** improvement in thruster usage
3. box confusion when the targets overlap even at different ranges
   1. 6 in start tracking other object immediately
   2. 12 in sames as for 6 inches
   3. 12 in same as before. Noted that the vehicle rapidly backs off after switching targets